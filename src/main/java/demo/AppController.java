package demo;

import java.security.Principal;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AppController {

    private static final String USER_MESSAGE = "Welcome user!";
    private static final String ADMIN_MESSAGE = "Hail to the king!";
    private final AtomicLong userCallCounter = new AtomicLong();
    private final AtomicLong adminCallCounter = new AtomicLong();

    @RequestMapping(value = "/")
    public ResponseEntity root(HttpServletRequest request, Principal principal) {
        ResponseEntity resp = null;
        if (principal == null) {
            resp = new ResponseEntity<>(new Message("Access denied"), HttpStatus.UNAUTHORIZED);
            return resp;
        }
        if ("user".equals(principal.getName())) {
            resp = new ResponseEntity<>(new MessageAndCount(userCallCounter.incrementAndGet(), USER_MESSAGE), HttpStatus.OK);
        }
        if ("admin".equals(principal.getName())) {
            resp = new ResponseEntity<>(new MessageAndCount(adminCallCounter.incrementAndGet(), ADMIN_MESSAGE), HttpStatus.OK);
        }

        if ("other".equals(principal.getName())) {
            resp = new ResponseEntity<>(new Message("User other does not have access"), HttpStatus.FORBIDDEN);
        }
        return resp;
    }
}