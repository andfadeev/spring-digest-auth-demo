package demo;

public class Message {

    private final String message;

    public Message(String content) {
        this.message = content;
    }

    public String getMessage() {
        return message;
    }
}