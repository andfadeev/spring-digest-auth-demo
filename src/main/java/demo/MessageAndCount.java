package demo;

public class MessageAndCount {

    private final long count;
    private final String message;

    public MessageAndCount(long count, String message) {
        this.count = count;
        this.message = message;
    }

    public long getCount() {
        return count;
    }

    public String getMessage() {
        return message;
    }
}